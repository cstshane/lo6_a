<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Supplier Update Results</title>
    </head>
    <body>
        <h1>Supplier Update Results</h1>
            
<?php

require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_A",
        __DIR__ . '/../../private/CSTClasses_A' );

use CSTClasses_A\DbObject;

unset( $_POST["submit"] );
unset( $_POST["_submitted"] );
var_dump( $_POST );

// Connect to the database
$db = new DbObject();

// Update the supplier in the database
$affectedRows = $db->update( $_POST, "Suppliers", "SupplierID" );

// Indicate to the user whether the update was successful
if ( $affectedRows > 0 )
{
    echo "<p>" . $_POST["CompanyName"] .
            " has been successfully updated.</p>\n";
    echo "<p><a href='6-SelectSupplier.php'>Click here</a> " .
            "to update another supplier.</p>\n";
}
else
{
    echo "<p>" . $_POST["CompanyName"] . " was not updated.</p>\n";
    print "<p><a href='#' onclick='history.back(); return false;'>" .
            "Click here</a> to try to update that supplier again.</p>\n";
}

?>

    </body>
</html>
