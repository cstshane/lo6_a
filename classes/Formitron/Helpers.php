<?php
namespace Formitron;

use Formitron\Element\BaseElement; 
/**
 * Static helper methods to make it easier to create submit blocks, and
 * so that creating items with a label doesn't take as much code
 *
 * @author ins208
 */
class Helpers
{
    public static function submitBlock($submitName, $submitText="Submit", $resetText=null)
    {
	$group = new Element\Group();
	$group->add(new Element\Button($submitName, $submitText, Element\Button::TYPE_SUBMIT));
	if(!is_null($resetText))
	{
	    $group->add(new Element\Button($resetText, $resetText, Element\Button::TYPE_RESET));
	}
	return $group;
    }
    
    /**
     * Creates a Formitron Group containing a label for the given element.
     * @param type $forName The value of the name attribute of $element
     * @param type $label The text label
     * @param BaseElement $element The element a label will be attached to
     * @return \Formitron\Element\Group A group containing a label and the
     *  given element
     */
    public static function withLabel($forName, $label, BaseElement $element)
    {
	$group = new Element\Group();
	$group->add(new Element\Label($forName, $label));
	$group->add($element);
	return $group;
    }

}
